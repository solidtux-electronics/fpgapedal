EESchema Schematic File Version 4
LIBS:fpgapedal-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 550  700  0    50   ~ 0
Flash
Wire Notes Line
	500  2000 500  600 
Wire Notes Line
	3050 2000 500  2000
Wire Notes Line
	3050 600  3050 2000
Wire Notes Line
	500  600  3050 600 
Connection ~ 1950 1550
Wire Wire Line
	2500 1550 1950 1550
Text GLabel 2500 1550 2    50   Input ~ 0
FLASH_WP
Connection ~ 2200 1450
Wire Wire Line
	2500 1450 2200 1450
Text GLabel 2500 1450 2    50   Input ~ 0
FLASH_MISO
Connection ~ 2450 1350
Wire Wire Line
	2500 1350 2450 1350
Connection ~ 2450 1250
Wire Wire Line
	2500 1250 2450 1250
Connection ~ 2200 1150
Wire Wire Line
	2500 1150 2200 1150
Connection ~ 1950 1050
Wire Wire Line
	1950 1050 2500 1050
Text GLabel 2500 1050 2    50   Input ~ 0
FLASH_RST
Text GLabel 2500 1150 2    50   Input ~ 0
FLASH_CS
$Comp
L Device:R R?
U 1 1 5D581ED4
P 2450 1750
F 0 "R?" H 2520 1796 50  0000 L CNN
F 1 "10k" H 2520 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2380 1750 50  0001 C CNN
F 3 "~" H 2450 1750 50  0001 C CNN
	1    2450 1750
	-1   0    0    -1  
$EndComp
Text GLabel 2550 1900 2    50   Input ~ 0
3V3
Wire Wire Line
	2200 1900 2450 1900
Connection ~ 2450 1900
Wire Wire Line
	2450 1900 2550 1900
Wire Wire Line
	2450 700  2200 700 
Connection ~ 2450 700 
Wire Wire Line
	2550 700  2450 700 
Text GLabel 2550 700  2    50   Input ~ 0
3V3
$Comp
L Device:R R?
U 1 1 5D581EE2
P 2450 850
F 0 "R?" H 2520 896 50  0000 L CNN
F 1 "10k" H 2520 805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2380 850 50  0001 C CNN
F 3 "~" H 2450 850 50  0001 C CNN
	1    2450 850 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2450 1600 2450 1350
Wire Wire Line
	2450 1350 1900 1350
Wire Wire Line
	1900 1250 2450 1250
Wire Wire Line
	2450 1250 2450 1000
Text GLabel 2500 1250 2    50   Input ~ 0
FLASH_CLK
Text GLabel 2500 1350 2    50   Input ~ 0
FLASH_MOSI
Connection ~ 2200 1900
Connection ~ 2200 700 
$Comp
L Device:R R?
U 1 1 5D581EF0
P 2200 1750
F 0 "R?" H 2270 1796 50  0000 L CNN
F 1 "10k" H 2270 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2130 1750 50  0001 C CNN
F 3 "~" H 2200 1750 50  0001 C CNN
	1    2200 1750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1950 1900 2200 1900
Wire Wire Line
	2200 700  1950 700 
$Comp
L Device:R R?
U 1 1 5D581EF8
P 2200 850
F 0 "R?" H 2270 896 50  0000 L CNN
F 1 "10k" H 2270 805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2130 850 50  0001 C CNN
F 3 "~" H 2200 850 50  0001 C CNN
	1    2200 850 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2200 1600 2200 1450
Wire Wire Line
	2200 1450 1900 1450
Wire Wire Line
	1900 1150 2200 1150
Wire Wire Line
	2200 1150 2200 1000
Wire Wire Line
	1950 1550 1900 1550
Wire Wire Line
	1950 1600 1950 1550
Wire Wire Line
	1950 1000 1950 1050
$Comp
L Device:R R?
U 1 1 5D581F05
P 1950 850
F 0 "R?" H 2020 896 50  0000 L CNN
F 1 "10k" H 2020 805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 850 50  0001 C CNN
F 3 "~" H 1950 850 50  0001 C CNN
	1    1950 850 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 1050 1950 1050
$Comp
L Device:R R?
U 1 1 5D581F0C
P 1950 1750
F 0 "R?" H 2020 1796 50  0000 L CNN
F 1 "10k" H 2020 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 1750 50  0001 C CNN
F 3 "~" H 1950 1750 50  0001 C CNN
	1    1950 1750
	-1   0    0    -1  
$EndComp
Connection ~ 950  1550
Wire Wire Line
	800  1550 950  1550
Wire Wire Line
	950  1250 950  1450
Connection ~ 950  1250
Wire Wire Line
	800  1250 950  1250
$Comp
L Device:C C?
U 1 1 5D581F17
P 800 1400
F 0 "C?" H 685 1354 50  0000 R CNN
F 1 "0.1u" H 685 1445 50  0000 R CNN
F 2 "" H 838 1250 50  0001 C CNN
F 3 "~" H 800 1400 50  0001 C CNN
	1    800  1400
	1    0    0    1   
$EndComp
Wire Wire Line
	950  1450 1000 1450
Wire Wire Line
	950  1150 950  1250
Text GLabel 950  1150 1    50   Input ~ 0
3V3
Wire Wire Line
	950  1550 950  1600
Wire Wire Line
	1000 1550 950  1550
$Comp
L solidtux:W25Q128JVS U?
U 1 1 5D581F22
P 1450 1300
F 0 "U?" H 1450 1815 50  0000 C CNN
F 1 "W25Q128JVS" H 1450 1724 50  0000 C CNN
F 2 "Package_SO:SOIC-8_5.23x5.23mm_P1.27mm" H 1450 800 50  0001 C CNN
F 3 "http://www.winbond.com/resource-files/w25q128jv_dtr%20revc%2003272018%20plus.pdf" H 1450 1400 50  0001 C CNN
	1    1450 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D581F28
P 950 1600
F 0 "#PWR?" H 950 1350 50  0001 C CNN
F 1 "GND" H 955 1427 50  0000 C CNN
F 2 "" H 950 1600 50  0001 C CNN
F 3 "" H 950 1600 50  0001 C CNN
	1    950  1600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
